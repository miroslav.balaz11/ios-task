//
//  Coordinator.swift
//  iosTask
//
//  Created by Miroslav Baláž on 05/06/2019.
//  Copyright © 2019 Miroslav Baláž. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
}
