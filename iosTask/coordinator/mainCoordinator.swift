//
//  mainCoordinator.swift
//  iosTask
//
//  Created by Miroslav Baláž on 05/06/2019.
//  Copyright © 2019 Miroslav Baláž. All rights reserved.
//

import Foundation
import UIKit

class mainCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = [Coordinator]()
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController){
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = loginViewController()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
        
    }
    
    func showBreweries(userId : Int){
        let vc = brewriesTableController()
        vc.coordinator = self
        vc.userId = userId
        vc.dataManager = breweryManager(userID: userId, delegate: vc)
        vc.dataManager?.fetchBreweries(skip: 0)
        
        navigationController.pushViewController(vc, animated: false)
    }
    
    func showBreweryWebsite(url: URL){
        let vc = websiteBrowserController()
        vc.coordinator = self
        vc.url = url
        
        navigationController.pushViewController(vc, animated: false)
    }
    
}
