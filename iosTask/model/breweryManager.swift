//
//  breweryManager.swift
//  iosTask
//
//  Created by Miroslav Baláž on 05/06/2019.
//  Copyright © 2019 Miroslav Baláž. All rights reserved.
//

import Foundation

class breweryManager {
    let userId : Int
    weak var delegate: brewriesTableController?
    
    init(userID: Int, delegate: brewriesTableController){
        self.userId = userID
        self.delegate = delegate
    }
    
    func fetchBreweries(skip: Int){
        let url = URL(string: "https://www.reintodev.cz:4006/api/breweries?userId=\(self.userId)&skip=\(skip)")!
        
        let request = URLRequest(url: url)
    
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            if let response = response {
                if let data = data {
                    self.delegate?.recieveData(data: data)
                }
            } else {
                self.delegate?.recieveData(error!)
                print(error ?? "Unknown error")
            }
            
        }
        
        task.resume()
    }
    
}
