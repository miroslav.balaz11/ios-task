//
//  loginManager.swift
//  iosTask
//
//  Created by Miroslav Baláž on 05/06/2019.
//  Copyright © 2019 Miroslav Baláž. All rights reserved.
//

import Foundation
import UIKit

class loginManager {
    let url = URL(string: "https://private-amnesiac-7426b9-ukol.apiary-proxy.com/api/login")!
    weak var delegate: loginViewController?
    
    init(){
        print("Login model created")
    }
    
    private func createHttpRequest(username name: String, password pswd: String) -> URLRequest{
        var request = URLRequest(url: self.url)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "content-type")
        
        request.httpBody = """
        {\n  \"login\": \"\(name)\",\n  \"password\": \"\(pswd)\"\n}
        """.data(using: .utf8)
        
        
        return request
    }
    
    private func validateServerData(response: HTTPURLResponse, data: Data) -> (Bool, Int){
        if ( response.statusCode == 200 ){
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Int]
                
                let userID = jsonData["userId"]!
                return (true, userID)
            
            } catch let error as NSError {
                print(error)
            }
        }
        
        return (false, -1)
    }
    
    private func informController(_ status: Bool, _ id: Int ){
        print("Debug: informing controller")
    }
    
    func fetchLoginData(username name: String, password pswd: String){
        let request = createHttpRequest(username: name, password: pswd)
        
        let task = URLSession.shared.dataTask(with: request){
        data, response, error in
            var status: Bool = false
            var userID: Int = -1
            
            if let response = response {
                
                if let data = data {
                    (status, userID) = self.validateServerData(response: response as! HTTPURLResponse, data: data)
                    
                }
            }
            
            self.delegate?.recievedData(id: userID, status: status)
        
    }
        
        task.resume()
        
    }
}

