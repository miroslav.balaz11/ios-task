//
//  loginViewController.swift
//  iosTask
//
//  Created by Miroslav Baláž on 05/06/2019.
//  Copyright © 2019 Miroslav Baláž. All rights reserved.
//

import UIKit

class loginViewController: UIViewController, UITextFieldDelegate {
    weak var coordinator: mainCoordinator?
    let loginDataManager : loginManager = loginManager()
    var userId : Int?

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        view.backgroundColor = UIColor.white;
        
        loginContent.addSubview(userNameField)
        loginContent.addSubview(passwordField)
        loginContent.addSubview(loginButton)
        
        view.addSubview(loginContent)
        
        setUpAutoConstraints()
        
        
        loginButton.addTarget(self, action: #selector(requestLogin), for: .touchUpInside)
        
        userNameField.delegate = self
        passwordField.delegate = self
        
        loginDataManager.delegate = self
        
    }
    
    func recievedData(id: Int, status: Bool){
        print("Here we are")
        if ( status ) {
            print("Loging is OK")
            self.userId = id
            DispatchQueue.main.sync {
                coordinator?.showBreweries(userId: self.userId!)
            }
            
        } else {
            DispatchQueue.main.sync{
                self.showToast(message: "Wrong password or username", duration: 1)
            }
        }
    }
    
    
    // Login view
    private let loginContent: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    // Username TextField for input
    private let userNameField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.borderStyle = .roundedRect
        textField.placeholder = "Enter your username"
        
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        return textField
    }()
    
    
    // Password textfield for input
    private let passwordField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.borderStyle = .roundedRect
        textField.placeholder = "Enter your password"
        textField.isSecureTextEntry = true
        
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        return textField
    }()
    
    // Login Button to confirm input
    private let loginButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor(red:0.69, green:0.87, blue:1.00, alpha:1.0)
        btn.setTitle("Login", for: .normal)
        btn.tintColor = .white
        btn.layer.cornerRadius = 5
        btn.clipsToBounds = true
        
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        return btn
    }()
    
    func setUpAutoConstraints(){
        // Constraints for login View holding all subViews for login
        loginContent.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        loginContent.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        loginContent.heightAnchor.constraint(equalToConstant: view.frame.height/3).isActive = true
        loginContent.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        
        
        /* Possitions for subViews */
        passwordField.topAnchor.constraint(equalTo: userNameField.bottomAnchor, constant: 30).isActive = true
        passwordField.centerXAnchor.constraint(equalTo: loginContent.centerXAnchor).isActive = true
        userNameField.centerXAnchor.constraint(equalTo: loginContent.centerXAnchor).isActive = true
        
        
        /* Width constraint for both Username and Password */
        passwordField.leftAnchor.constraint(equalTo: loginContent.leftAnchor, constant: 30).isActive = true
        passwordField.rightAnchor.constraint(equalTo: loginContent.rightAnchor, constant: -30).isActive = true
        
        userNameField.leftAnchor.constraint(equalTo: loginContent.leftAnchor, constant: 30).isActive = true
        userNameField.rightAnchor.constraint(equalTo: loginContent.rightAnchor, constant: -30).isActive = true
        
        /* Login Button constraints */
        loginButton.centerXAnchor.constraint(equalTo: loginContent.centerXAnchor).isActive = true
        loginButton.topAnchor.constraint(equalTo: passwordField.bottomAnchor, constant: 30).isActive = true
        loginButton.leftAnchor.constraint(equalTo: loginContent.leftAnchor, constant: 80).isActive = true
        loginButton.rightAnchor.constraint(equalTo: loginContent.rightAnchor, constant: -80).isActive = true
        
        /* Height for views */
        passwordField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        userNameField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        textField.becomeFirstResponder()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        print("Debug: Should return and resign responder")
        
        return true
    }
    
    @objc
    private func requestLogin(_ sender: AnyObject?){
        guard let username = self.userNameField.text, !username.isEmpty else {
            print("No username")
            return
        }
        
        guard let password = self.passwordField.text, !password.isEmpty else {
            print("No password")
            return
        }
        
        
        self.loginDataManager.fetchLoginData(username: username, password: password)
        
    }
    
    
    
    private func showToast(message msg: String, duration seconds: Double){
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alert.view.backgroundColor = .black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        
        self.present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds, execute: {
            alert.dismiss(animated: true)
        })
    }
    
}
