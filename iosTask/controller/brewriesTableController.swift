//
//  brewriesTableController.swift
//  iosTask
//
//  Created by Miroslav Baláž on 05/06/2019.
//  Copyright © 2019 Miroslav Baláž. All rights reserved.
//

import UIKit

struct brewery {
    var id : Int
    var name: String
    var url:  URL
}

class brewriesTableController: UITableViewController {
    var breweries : [brewery] = [brewery]() // DataSource
    
    var userId : Int?
    private let cellId = "brewCell"
    var dataManager: breweryManager?
    
    weak var coordinator: mainCoordinator?
    
    var  stopDownloading : Bool = false;
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupTableView()

    }
    
    func setupTableView(){
        tableView.register(breweryCell.self, forCellReuseIdentifier: self.cellId)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.breweries.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId, for: indexPath) as! breweryCell

        let brew = self.breweries[indexPath.row]
        let name = brew.name
        let url = brew.url
        
        cell.setValues(name, url, self)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if ( self.breweries.count - 1 == indexPath.row ){
            print("Have \(self.breweries.count), donwloading new")
            self.dataManager?.fetchBreweries(skip: self.breweries.count)
        }
    }
    
    /* Processing data fetched from API */
    func recieveData(data: Data?){
        do {
            let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [Any]
            
            if ( jsonData.isEmpty ) {
                self.stopDownloading = true
                return
            }
            
            var id: Int
            var name: String
            var url: URL
            
            var item: [String: Any]
            var brew : brewery
            
            for items in jsonData {
                item = items as! [String: Any]
                name = item["name"] as! String
                id = item["id"] as! Int
                url = URL(string: item["url"] as! String)!
                
                brew = brewery(id: id, name: name, url: url)
                self.breweries.append(brew)
                
            }
            
            DispatchQueue.main.sync {
                self.tableView.reloadData()
            }
            
            print("Fetched new data -> \(self.breweries.count)")
            
        } catch let error {
            print(error)
        }
    }
    
    func recieveData(_ error: Error){
        print(error)
    }
    
    func createWebPreview(webURL url: URL){
        print("Button clicked")
        self.coordinator?.showBreweryWebsite(url: url)
    }

}

class breweryCell: UITableViewCell {
    var cellDelegate: brewriesTableController?
    var name : String?
    var url : URL?
    
    func setValues(_ name: String, _ url: URL, _ delegate: UITableViewController){
        self.name = name
        self.url = url
        self.cellDelegate = delegate as! brewriesTableController
        self.breweryName.text = self.name
    }
    
    @objc
    func webButtonPressed(_ sender: AnyObject?){
        self.cellDelegate!.createWebPreview(webURL: self.url!)
    }
    
    private let breweryName: UILabel = {
        let label = UILabel()
        
        label.backgroundColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Cell"
        
        return label
    }()
    
    private let urlButton : UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor(red:0.69, green:0.87, blue:1.00, alpha:1.0)
        btn.setTitle("Web", for: .normal)
        btn.tintColor = .white
        btn.layer.cornerRadius = 5
        btn.clipsToBounds = true
        
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        return btn
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = .white
        
        contentView.addSubview(breweryName)
        contentView.addSubview(urlButton)
        
        self.urlButton.addTarget(self, action: #selector(webButtonPressed), for: .touchUpInside)
        
        setConstrains()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    private func setConstrains(){
        breweryName.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        
        breweryName.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        breweryName.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        urlButton.widthAnchor.constraint(equalToConstant: contentView.frame.width / 3).isActive = true
        
        
        urlButton.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        
        
        urlButton.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        
        urlButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        breweryName.rightAnchor.constraint(equalTo: urlButton.leftAnchor).isActive = true
        
    }
}
