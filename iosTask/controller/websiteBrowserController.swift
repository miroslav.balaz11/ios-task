//
//  websiteBrowserController.swift
//  iosTask
//
//  Created by Miroslav Baláž on 05/06/2019.
//  Copyright © 2019 Miroslav Baláž. All rights reserved.
//

import UIKit
import WebKit

class websiteBrowserController: UIViewController, WKUIDelegate {

    var webView: WKWebView!
    var url: URL?
    var coordinator: mainCoordinator?
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func loadView() {
        webView = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
        self.view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Loading page")
        let myRequest = URLRequest(url: self.url!)
        webView.load(myRequest)
        
        print(self.url!)
        webView.allowsBackForwardNavigationGestures = true
    }

}
